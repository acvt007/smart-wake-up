package com.example.android.smartwakeup;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import it.sephiroth.android.library.numberpicker.NumberPicker;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE = 130592;
    static final int REQUEST_AUDIO_GET = 200799;

    Button twoBtn, threeBtn, fourBtn, fiveBtn, sixBtn, sevenBtn, cancelBtn, okBtn;
    TextView wakeUpTimeTV;
    NumberPicker waitingTimePicker;
    private AlarmManager alarmManager;
    private SharedPrefs sharedPrefs;
    int[] ids = {R.id.btn_two, R.id.btn_three, R.id.btn_four, R.id.btn_five, R.id.btn_six, R.id.btn_seven};
    long wakeUpTotalTime = 0;
    int waitingTime = 14;
    long wakeupTime = 0;
    int number = 0;
    int id = 0;
    int ONE_MINUTE = 60*1000;
    int ONE_CYCLE = 90*60*1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        TextView title = findViewById(R.id.text_title);
        title.setText(getString(R.string.wakeup));
        sharedPrefs = SharedPrefs.getInstance(MainActivity.this);
        waitingTime = sharedPrefs.getWaitingTime();
        wakeupTime = sharedPrefs.getWakeupTime();
        number = sharedPrefs.getCycleTime();
        id = sharedPrefs.getCycleId();

        twoBtn = findViewById(R.id.btn_two);
        threeBtn = findViewById(R.id.btn_three);
        fourBtn = findViewById(R.id.btn_four);
        fiveBtn = findViewById(R.id.btn_five);
        sixBtn = findViewById(R.id.btn_six);
        sevenBtn = findViewById(R.id.btn_seven);
        cancelBtn = findViewById(R.id.btn_cancel);
        wakeUpTimeTV = findViewById(R.id.wakeup_time);
        okBtn = findViewById(R.id.btn_ok);
        waitingTimePicker = findViewById(R.id.waiting_time);
        waitingTimePicker.setMinValue(0);
        waitingTimePicker.setMaxValue(90);

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        waitingTimePicker.setProgress(waitingTime);
        waitingTimePicker.setNumberPickerChangeListener(new NumberPicker.OnNumberPickerChangeListener() {
            @Override
            public void onProgressChanged(it.sephiroth.android.library.numberpicker.@NotNull NumberPicker numberPicker, int i, boolean b) {
                waitingTime = i;
                sharedPrefs.setWaitingTime(waitingTime);
                totalTime();
            }

            @Override
            public void onStartTrackingTouch(it.sephiroth.android.library.numberpicker.@NotNull NumberPicker numberPicker) {

            }

            @Override
            public void onStopTrackingTouch(it.sephiroth.android.library.numberpicker.@NotNull NumberPicker numberPicker) {

            }
        });


        twoBtn.setOnClickListener(onClickListener);
        threeBtn.setOnClickListener(onClickListener);
        fourBtn.setOnClickListener(onClickListener);
        fiveBtn.setOnClickListener(onClickListener);
        sixBtn.setOnClickListener(onClickListener);
        sevenBtn.setOnClickListener(onClickListener);
        okBtn.setVisibility(wakeupTime > 0 ? View.GONE : View.VISIBLE);
        cancelBtn.setVisibility(wakeupTime > 0 ? View.VISIBLE : View.GONE);
        totalTime();
        showCycleButton(id);
        updateTimeUI(wakeupTime);
    }



    View.OnClickListener onClickListener = view -> {
        if (cancelBtn.getVisibility() == View.VISIBLE) return;
        id = view.getId();
        switch (id) {
            case R.id.btn_two:
                number = 2;
                break;
            case R.id.btn_three:
                number = 3;
                break;
            case R.id.btn_four:
                number = 4;
                break;
            case R.id.btn_five:
                number = 5;
                break;
            case R.id.btn_six:
                number = 6;
                break;
            case R.id.btn_seven:
                number = 7;
                break;
            default:
                number = 0;
                break;
        }
        btnClick(id);

    };

    private void btnClick(int idSelected) {
        showCycleButton(idSelected);
        totalTime();
    }

    private void showCycleButton(int idSelected) {
        for (int id : ids) {
            findViewById(id).setBackgroundColor(getResources().getColor(id == idSelected ? R.color.green_500 : R.color.colorPrimary));
        }
    }

    private void totalTime() {
        wakeUpTotalTime = waitingTime * ONE_MINUTE + number * ONE_CYCLE;
        int h = (int) wakeUpTotalTime / 60 /ONE_MINUTE;
        int m = (int) (wakeUpTotalTime - h*60*ONE_MINUTE) / ONE_MINUTE;
        updateTimeUI(System.currentTimeMillis() + wakeUpTotalTime);
    }

    private void updateTimeUI(long wakeupTime) {
        if (wakeupTime == 0) {
            wakeUpTimeTV.setText("None");
            return;
        }
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss, dd/MM ");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(wakeupTime);
        wakeUpTimeTV.setText(formatter.format(calendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.menu_alarm, menu);
        return true;
    }

    public void startTimer(View view) {
        okBtn.setVisibility(View.GONE);
        cancelBtn.setVisibility(View.VISIBLE);
        long goOffPoint = System.currentTimeMillis() + wakeUpTotalTime;
        sharedPrefs.setCycleId(id);
        sharedPrefs.setCycleTime(number);
        sharedPrefs.setWakeupTime(goOffPoint);
        updateTimeUI(goOffPoint);
        PendingIntent pendingIntent = getPendingIntent();
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, goOffPoint, pendingIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, goOffPoint, pendingIntent);
            }
        }
    }

    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        return PendingIntent.getBroadcast(this, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void cancelTimer(View view) {
        number = 0;
        wakeupTime = 0;
        sharedPrefs.setCycleTime(0);
        sharedPrefs.setCycleId(0);
        sharedPrefs.setWakeupTime(0);
        waitingTimePicker.setProgress(waitingTime);
        btnClick(-1);
        wakeUpTimeTV.setText("None");
        alarmManager.cancel(getPendingIntent());
        okBtn.setVisibility(View.VISIBLE);
        cancelBtn.setVisibility(View.GONE);
    }

    public void selectAudio() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_AUDIO_GET);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_AUDIO_GET && resultCode == RESULT_OK) {

            // Do work with photo saved at fullPhotoUri
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
