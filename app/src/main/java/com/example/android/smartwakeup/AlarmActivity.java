package com.example.android.smartwakeup;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AlarmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.friends_of_shen_yun_org);
        mediaPlayer.start();
        findViewById(R.id.notification_alarm).setOnClickListener(v -> {
            mediaPlayer.stop();
            finish();
        });
    }
}
