package com.example.android.smartwakeup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("AlarmReceiver", "onReceive");
        Toast.makeText(context, "Alarm Triggered", Toast.LENGTH_SHORT).show();
//        context.startService(new Intent(context, MediaPlaybackService.class));
        SharedPrefs.getInstance(context).setCycleTime(0);
        SharedPrefs.getInstance(context).setCycleId(0);
        SharedPrefs.getInstance(context).setWakeupTime(0);
        context.startActivity(new Intent(context, AlarmActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
