package com.example.android.smartwakeup;


import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {

    private static final String WAKEUP_APP_SHARED_PREFERENCES_STORE = "WakeUpAppPrefs";

    private static final String WAITING_TIME = "waiting_time";
    private static final String WAKEUP_TIME = "wakeup_time";
    private static final String CYCLE_TIME = "cycle_time";
    private static final String CYCLE_ID = "cycle_id";

    private static SharedPrefs sEttNewsPrefs;
    private SharedPreferences mSharedPrefs;
    private SharedPreferences.Editor mPrefEditor;


    private SharedPrefs(Context ctx) {
        // 0 - for private mode
        mSharedPrefs = ctx.getSharedPreferences(WAKEUP_APP_SHARED_PREFERENCES_STORE, 0);
        mPrefEditor = mSharedPrefs.edit();
    }

    public static SharedPrefs getInstance(Context ctx) {
        if (sEttNewsPrefs == null) {
            synchronized (SharedPrefs.class) {
                if (sEttNewsPrefs == null) {
                    sEttNewsPrefs = new SharedPrefs(ctx);
                }
            }
        }

        return sEttNewsPrefs;
    }

    public void clearAll() {
        mPrefEditor.clear();
        mPrefEditor.commit();
    }

    public int getWaitingTime() {
        return mSharedPrefs.getInt(WAITING_TIME, 14);
    }

    public void setWaitingTime(int waitingTime) {
        mSharedPrefs.edit().putInt(WAITING_TIME, waitingTime).apply();
    }

    public int getCycleTime() {
        return mSharedPrefs.getInt(CYCLE_TIME, 2);
    }

    public void setCycleTime(int cycleTime) {
        mSharedPrefs.edit().putInt(CYCLE_TIME, cycleTime).apply();
    }

    public int getCycleId() {
        return mSharedPrefs.getInt(CYCLE_ID, R.id.btn_two);
    }

    public void setCycleId(int cycleId) {
        mSharedPrefs.edit().putInt(CYCLE_ID, cycleId).apply();
    }

    public long getWakeupTime() {
        return mSharedPrefs.getLong(WAKEUP_TIME, 0);
    }

    public void setWakeupTime(long wakeupTime) {
        mSharedPrefs.edit().putLong(WAKEUP_TIME, wakeupTime).apply();
    }

}
